package com.test.consumeapiservice.api;

import com.google.gson.GsonBuilder;
import com.test.consumeapiservice.api.deserializer.MyDeserializer;
import com.test.consumeapiservice.models.City;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class API {

    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String APPID = "1a977b7e5d3dc9f00045881e91a78505";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit() {
        if (retrofit == null) {

            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeAdapter(City.class, new MyDeserializer());

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(builder.create()))
                    .build();
        }
        return retrofit;
    }
}
