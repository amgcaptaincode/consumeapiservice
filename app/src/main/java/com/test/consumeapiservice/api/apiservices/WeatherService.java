package com.test.consumeapiservice.api.apiservices;

import com.test.consumeapiservice.models.City;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherService {

    @GET("weather")
    Call<City> currentWeather(@Query("q") String city, @Query("appid") String appid);

    @GET("weather")
    Call<City> currentWeatherCelsius(@Query("q") String city, @Query("appid") String appid, @Query("units") String value);
}
